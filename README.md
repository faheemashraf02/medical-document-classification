# Medical Document Classification

The purpose of this project was to classify and extract data from medical documents shared by users. For classification of document, a model in BERT was developed and rest of the data was extracted by AWS Textract and AWS Medical Comprehend using Named Entity Recognition(NER).
BERT stands for Bidirectional Encoder Representations from Transformers. It is designed to pre-train deep bidirectional representations from unlabeled text by jointly conditioning on both left and right context. The data used in this project is the Medical-Abstracts-TC-Corpus dataset. The dataset has 5 categories - Neoplasms, Digestive, Cardiovascular, Nervous and General pathological conditions. The dataset had 14438 annotated samples.

# Medical-Abstracts-TC-Corpus
The medical abstracts dataset has 5 different classes of patient conditions. The dataset can be used for text classification. 

Summary of the medical abstracts dataset:


| **Class name**                  | **#training** | **#test** | **Total** |
|---------------------------------|---------------|-----------|-----------|
| Neoplasms                       | 2530          | 633       | 3163      |
| Digestive system diseases       | 1195          | 299       | 1494      |
| Nervous system diseases         | 1540          | 385       | 1925      |
| Cardiovascular diseases         | 2441          | 610       | 3051      |
| General pathological conditions | 3844          | 961       | 4805      |
| **Total**                       | **11550**     | **2888**  | **14438** |

![alt text](https://media.istockphoto.com/photos/medical-records-patient-charts-personnel-files-picture-id134544878)
